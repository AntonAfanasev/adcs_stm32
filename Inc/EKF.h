#pragma once

#include <armadillo>

class EKF
{
public:
	/* getters and setters */
	double get_sigma_q0();
	void set_sigma_q0(double new_sigma_q0);
	double get_sigma_omega0();
	void set_sigma_omega0(double new_sigma_omega0);
	double get_sigma_mres0();
	void set_sigma_mres0(double new_sigma_mres0);
	double get_sigma_mmbias0();
	void set_sigma_mmbias0(double new_sigma_mmbias0);
	double get_sigma_gyrbias0();
	void set_sigma_gyrbias0(double new_sigma_gyrbias0);
	arma::Mat<double> get_P_corr();
	void set_P_corr(arma::Mat<double> new_P_corr);
	double get_mean_motion();
	void set_mean_motion(double new_mean_motion);
	arma::Mat<double> get_F();
	void set_F(arma::Mat<double> new_F);
	arma::Mat<double> get_Fi();
	void set_Fi(arma::Mat<double> new_Fi);
	arma::Mat<double> get_Sigma_model();
	void set_Sigma_model(arma::Mat<double> new_Sigma_model);
	arma::Mat<double> get_P_pred();
	void set_P_pred(arma::Mat<double> new_P_pred);
	arma::Mat<double> get_H_MSG();
	void set_H_MSG(arma::Mat<double> new_H_MSG);
	arma::Mat<double> get_H_MG();
	void set_H_MG(arma::Mat<double> new_H_MG);
	arma::Mat<double> get_K_MSG();
	void set_K_MSG(arma::Mat<double> new_K_MSG);
	arma::Mat<double> get_K_MG();
	void set_K_MG(arma::Mat<double> new_K_MG);
	arma::Col<double> get_Z_MSG();
	void set_Z_MSG(arma::Col<double> new_Z_MSG);
	arma::Col<double> get_Z_MG();
	void set_Z_MG(arma::Col<double> new_Z_MG);
	arma::Col<double> get_h_MSG();
	void set_h_MSG(arma::Col<double> new_h_MSG);
	arma::Col<double> get_h_MG();
	void set_h_MG(arma::Col<double> new_h_MG);
	arma::Mat<double> get_J();
	void set_J(arma::Mat<double> new_J);
	arma::Mat<double> get_J_inv();
	void set_J_inv(arma::Mat<double> new_J_inv);
	arma::Mat<double> get_delta_J_cycl();
	void set_delta_J_cycl(arma::Mat<double> new_delta_J_cycl);
	double get_sigma_torque();
	void set_sigma_torque(double new_sigma_torque);
	double get_sigma_mres();
	void set_sigma_mres(double new_sigma_mres);
	double get_sigma_mm_bias();
	void set_sigma_mm_bias(double new_sigma_mm_bias);
	double get_sigma_gyr_bias();
	void set_sigma_gyr_bias(double new_sigma_gyr_bias);
	double get_sigma_magn();
	void set_sigma_magn(double new_sigma_magn);
	double get_sigma_sun();
	void set_sigma_sun(double new_sigma_sun);
	double get_sigma_gyr();
	void set_sigma_gyr(double new_sigma_gyr);
	arma::Mat<double> get_Sigma_process();
	void set_Sigma_process(arma::Mat<double> new_Sigma_process);
	arma::Mat<double> get_G();
	void set_G(arma::Mat<double> new_G);
	arma::Mat<double> get_Sigma_obs_MSG();
	void set_Sigma_obs_MSG(arma::Mat<double> new_Sigma_obs_MSG);
	arma::Mat<double> get_Sigma_obs_MG();
	void set_Sigma_obs_MG(arma::Mat<double> new_Sigma_obs_MG);
	arma::Col<double> get_X();
	void set_X(arma::Col<double> new_X);
	bool get_shadow();
	void set_shadow(bool new_shadow);
	arma::Col<double> get_B_body();
	void set_B_body(arma::Col<double> new_B_body);

	/* initializing matrices */
	void set_sigmas_init(double s_q0, double s_w0, double s_mres0, double s_mmbias0, double s_gyrbias0, double s_torque, double s_mres, double s_mm_bias, double s_gyr_bias, double s_magn, double s_sun, double s_gyr);
	void set_P_init();
	void set_J_init(arma::Col<double> J_vec);
	void set_Sigma_init();
	void update_F(double T, arma::Col<double> m_body_prev);
	void update_Fi_and_Sigma_model(double T);
	void update_P_pred();
	void update_H_MSG(arma::Col<double> B_body_model, arma::Col<double> S_body_model);
	void update_H_MG(arma::Col<double> B_body_model);
	void update_K_MSG();
	void update_K_MG();
	void update_Z_MSG(arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas);
	void update_Z_MG(arma::Col<double> B_meas, arma::Col<double> omega_meas);
	void update_h_MSG(arma::Col<double> B_body_model, arma::Col<double> S_body_model, arma::Col<double> omega_body_model);
	void update_h_MG(arma::Col<double> B_body_model, arma::Col<double> omega_body_model);
	void update_P_corr_MSG();
	void update_P_corr_MG();
	void update_X_MSG(arma::Col<double> X_extr);
	void update_X_MG(arma::Col<double> X_extr);
	void correct_state_MSG(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> S_orb_model, arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev);
	void correct_state_MG(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> B_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev);
	void update_shadow(arma::Col<double> S_model, arma::Col<double> R);
	void correct_state(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> S_orb_model, arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev, arma::Col<double> R_orb);

private:
	double sigma_q0 = 0;
	double sigma_omega0 = 0;
	double sigma_mres0 = 0;
	double sigma_mmbias0 = 0;
	double sigma_gyrbias0 = 0;
	arma::Mat<double> P_corr = arma::Mat<double>(15, 15, arma::fill::zeros);

	double mean_motion = 0;
	arma::Mat<double> F = arma::Mat<double>(15, 15, arma::fill::zeros);
	arma::Mat<double> Fi = arma::Mat<double>(15, 15, arma::fill::zeros);
	arma::Mat<double> Sigma_model = arma::Mat<double>(15, 15, arma::fill::zeros);
	arma::Mat<double> P_pred = arma::Mat<double>(15, 15, arma::fill::zeros);
	arma::Mat<double> H_MSG = arma::Mat<double>(9, 15, arma::fill::zeros);
	arma::Mat<double> H_MG = arma::Mat<double>(6, 15, arma::fill::zeros);
	arma::Mat<double> K_MSG = arma::Mat<double>(15, 9, arma::fill::zeros);
	arma::Mat<double> K_MG = arma::Mat<double>(15, 6, arma::fill::zeros);
	arma::Col<double> Z_MSG = arma::Col<double>(9, arma::fill::zeros);
	arma::Col<double> Z_MG = arma::Col<double>(6, arma::fill::zeros);
	arma::Col<double> h_MSG = arma::Col<double>(9, arma::fill::zeros);
	arma::Col<double> h_MG = arma::Col<double>(6, arma::fill::zeros);

	arma::Mat<double> J = arma::Mat<double>(3, 3, arma::fill::zeros);
	arma::Mat<double> J_inv = arma::Mat<double>(3, 3, arma::fill::zeros);
	arma::Mat<double> delta_J_cycl = arma::Mat<double>(3, 3, arma::fill::zeros);

	double sigma_torque = 0;
	double sigma_mres = 0;
	double sigma_mm_bias = 0;
	double sigma_gyr_bias = 0;

	double sigma_magn = 0;
	double sigma_sun = 0;
	double sigma_gyr = 0;

	arma::Mat<double> Sigma_process = arma::Mat<double>(12, 12, arma::fill::zeros);
	arma::Mat<double> G = arma::Mat<double>(15, 12, arma::fill::zeros);
	arma::Mat<double> Sigma_obs_MSG = arma::Mat<double>(9, 9, arma::fill::zeros);
	arma::Mat<double> Sigma_obs_MG = arma::Mat<double>(6, 6, arma::fill::zeros);

	arma::Col<double> X = arma::Col<double> (16, arma::fill::zeros);

	arma::Col<double> B_body = arma::Col<double>(3, arma::fill::zeros);

	bool inShadow = false;
};
