/*
 * imu.h
 *
 *  Created on: 7 нояб. 2019 г.
 *      Author: ashim
 */

#ifndef IMU_H_
#define IMU_H_

#ifdef __cplusplus
 extern "C" {
#endif

#define DEV_ADDR 0x3C
#define DEV_SETTINGS_A 0b01110000
#define DEV_SETTINGS_B 0b10100000
#define DEV_SETTINGS_C 0b00000000

#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"

void i2cread(uint8_t device);
void i2cwrite(uint8_t device, uint8_t shift, uint8_t data, uint8_t size);
void initImu(I2C_HandleTypeDef *hi2c, UART_HandleTypeDef * debug_uart);
void getValue(int16_t * mag_x, int16_t * mag_y, int16_t * mag_z, int16_t * gyro_x, int16_t * gyro_y, int16_t * gyro_z, int16_t * accel_x, int16_t * accel_y, int16_t * accel_z, int16_t * unc_mx, int16_t * unc_my, int16_t * unc_mz);

#ifdef __cplusplus
}
#endif

#endif /* IMU_H_ */
