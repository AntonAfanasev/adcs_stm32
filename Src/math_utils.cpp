/*
 * math_utils.cpp
 *
 *  Created on: 21 апр. 2021 г.
 *      Author: Anton
 */




#include "math_utils.h"

arma::Mat<double> skew_symm(arma::Col<double> vec)
{
	arma::Mat<double> W(3, 3, arma::fill::zeros);
	W(0, 1) = -vec(2);
	W(0, 2) = vec(1);
	W(1, 0) = vec(2);
	W(1, 2) = -vec(0);
	W(2, 0) = -vec(1);
	W(2, 1) = vec(0);
	return W;
}

arma::Mat<double> symm(arma::Col<double> vec)
{
	arma::Mat<double> V(3, 3, arma::fill::zeros);
	V(0, 1) = vec(2);
	V(0, 2) = vec(1);
	V(1, 0) = vec(2);
	V(1, 2) = vec(0);
	V(2, 0) = vec(1);
	V(2, 1) = vec(0);
	return V;
}

arma::Col<double> quat_product(arma::Col<double> q1, arma::Col<double> q2)
{
	arma::Col<double> new_q({ 0,0,0,0 });

	new_q(0) = q1(0) * q2(0) - arma::dot(q1.subvec(1, 3), q2.subvec(1, 3));
	new_q.subvec(1, 3) = q1(0) * q2.subvec(1, 3) + q2(0) * q1.subvec(1, 3) + arma::cross(q1.subvec(1, 3), q2.subvec(1, 3));

	return new_q;
}

arma::Col<double> rot_q(arma::Col<double> q, arma::Col<double> vec)
{
	arma::Col<double> q_vec = q.subvec(1, 3);
	arma::Col<double> qxvec = arma::cross(q_vec, vec);

	arma::Col<double> new_vec = arma::dot(q_vec, vec) * q_vec + pow(q(0), 2) * vec + arma::cross(q_vec, qxvec) - 2 * q(0) * qxvec;

	return new_vec;
}

arma::Col<double> omega_to_Omega(arma::Col<double> omega, arma::Col<double> quat, double mean_motion)
{
	arma::Col<double> mm_vec({ 0, mean_motion, 0 });

	arma::Col<double> Omega = omega - rot_q(quat, mm_vec);

	return Omega;
}

arma::Col<double> produce_moment(double Kw, double Ks, double mean_motion, arma::Col<double> S, arma::Col<double> Omega, arma::Col<double> B_body_model, arma::Col<double> m_res_est)
{
	//arma::Col<double> m = Kw / mean_motion * arma::cross(Omega, B_body_model) + Ks * arma::cross(S, B_body_model) - m_res_est;
	arma::Col<double> m = Kw * arma::cross(Omega, B_body_model) + Ks * arma::cross(S, B_body_model) - m_res_est;

	return m;
}

arma::Col<double> cumulative_torque(arma::Col<double> quat, arma::Mat<double> J, double mean_motion, double sigma_torque, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl)
{
	arma::Col<double> eZ = rot_q(quat, arma::Col<double>({ 0, 0, 1 }));
	arma::Col<double> d_Torque = arma::randn(3) * sigma_torque;
	arma::Col<double> c_Torque = arma::cross(m_moment, B_body_true);

	return isCtrl * c_Torque + 0 * 3 * pow(mean_motion, 2) * arma::cross(eZ, J * eZ) + d_Torque;
	//return isCtrl * c_Torque + 3 * pow(mean_motion, 2) * arma::cross(eZ, J * eZ) + d_Torque;
}

arma::Col<double> rk4_step(arma::Col<double> X_n, double h, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl, std::function<arma::Col<double>(arma::Col<double>, double, double, arma::Mat<double>, arma::Mat<double>, arma::Col<double>, arma::Col<double>, bool)> func)
{
	arma::Col<double> vec1 = func(X_n, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl);
	arma::Col<double> vec2 = func(X_n + h / 2 * vec1, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl);
	arma::Col<double> vec3 = func(X_n + h / 2 * vec2, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl);
	arma::Col<double> vec4 = func(X_n + h * vec3, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl);

	arma::Col<double> X_n1 = X_n + h / 6 * (vec1 + 2 * vec2 + 2 * vec3 + vec4);

	return X_n1;
}

arma::Col<double> rk4(arma::Col<double> X_0, double t, const int n, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl, std::function<arma::Col<double>(arma::Col<double>, double, double, arma::Mat<double>, arma::Mat<double>, arma::Col<double>, arma::Col<double>, bool)> func)
{
	double h = t / n;

	arma::Col<double> X_i = X_0;
	arma::Col<double> X_i1 = X_0;

	for (int i = 0; i < n; i++)
	{
		X_i1 = rk4_step(X_i, h, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl, func);
		X_i = X_i1;
	}

	return X_i;
}

arma::Col<double> motion_equation(arma::Col<double> x, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl)
{
	arma::Col<double> quat = x.subvec(0, 3);
	arma::Col<double> omega = x.subvec(4, 6);

	arma::Col<double> Omega = omega_to_Omega(omega, quat, mean_motion);
	arma::Col<double> Torque = cumulative_torque(quat, J, mean_motion, sigma_torque, m_moment, B_body_true, isCtrl);

	arma::Col<double> x_dot(16, arma::fill::zeros);

	x_dot.subvec(4, 6) = J_inv * (Torque - arma::cross(omega, J * omega));
	x_dot(0) = -0.5 * arma::dot(quat.subvec(1,3), Omega);
	x_dot.subvec(1, 3) = 0.5 * (quat(0) * Omega + arma::cross(quat.subvec(1,3), Omega));
	x_dot.subvec(7, 15) = x.subvec(7, 15);

	return x_dot;
}
arma::Col<double> solve_motion_equation(arma::Col<double> state, double t, const int n, double mean_motion, double sigma_torque, arma::Mat<double> J, arma::Mat<double> J_inv, arma::Col<double> m_moment, arma::Col<double> B_body_true, bool isCtrl)
{
	arma::Col<double> new_state = rk4(state, t, n, mean_motion, sigma_torque, J, J_inv, m_moment, B_body_true, isCtrl, motion_equation);

	arma::Col<double> new_quat = arma::normalise(new_state.subvec(0, 3));
	new_state.subvec(0, 3) = new_quat;

	return new_state;
}

arma::Col<double> moment_scale(arma::Col<double> m, double max)
{
	double m0 = 0;
	double m1 = 0;
	double m2 = 0;

	if (m(0) <= -max)
	{
		m0 = -max;
	}
	else if (m(0) >= max)
	{
		m0 = max;
	}
	else
	{
		m0 = m(0);
	}
	if (m(1) <= -max)
	{
		m1 = -max;
	}
	else if (m(1) >= max)
	{
		m1 = max;
	}
	else
	{
		m1 = m(1);
	}
	if (m(2) <= -max)
	{
		m2 = -max;
	}
	else if (m(2) >= max)
	{
		m2 = max;
	}
	else
	{
		m2 = m(2);
	}

	return arma::Col<double>{m0,m1,m2};
}
