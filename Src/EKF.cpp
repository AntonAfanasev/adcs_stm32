/*
 * EKF.cpp
 *
 *  Created on: 21 апр. 2021 г.
 *      Author: Anton
 */




#include "EKF.h"
#include "math_utils.h"

double EKF::get_sigma_q0()
{
	return sigma_q0;
}

void EKF::set_sigma_q0(double new_sigma_q0)
{
	sigma_q0 = new_sigma_q0;
}

double EKF::get_sigma_omega0()
{
	return sigma_omega0;
}

void EKF::set_sigma_omega0(double new_sigma_omega0)
{
	sigma_omega0 = new_sigma_omega0;
}

double EKF::get_sigma_mres0()
{
	return sigma_mres0;
}

void EKF::set_sigma_mres0(double new_sigma_mres0)
{
	sigma_mres0 = new_sigma_mres0;
}

double EKF::get_sigma_mmbias0()
{
	return sigma_mmbias0;
}

void EKF::set_sigma_mmbias0(double new_sigma_mmbias0)
{
	sigma_mmbias0 = new_sigma_mmbias0;
}

double EKF::get_sigma_gyrbias0()
{
	return sigma_gyrbias0;
}

void EKF::set_sigma_gyrbias0(double new_sigma_gyrbias0)
{
	sigma_gyrbias0 = new_sigma_gyrbias0;
}

arma::Mat<double> EKF::get_P_corr()
{
	return P_corr;
}

void EKF::set_P_corr(arma::Mat<double> new_P_corr)
{
	P_corr = new_P_corr;
}
double EKF::get_mean_motion()
{
	return mean_motion;
}
void EKF::set_mean_motion(double new_mean_motion)
{
	mean_motion = new_mean_motion;
}
arma::Mat<double> EKF::get_F()
{
	return F;
}
void EKF::set_F(arma::Mat<double> new_F)
{
	F = new_F;
}
arma::Mat<double> EKF::get_Fi()
{
	return Fi;
}
void EKF::set_Fi(arma::Mat<double> new_Fi)
{
	Fi = new_Fi;
}
arma::Mat<double> EKF::get_Sigma_model()
{
	return Sigma_model;
}
void EKF::set_Sigma_model(arma::Mat<double> new_Sigma_model)
{
	Sigma_model = new_Sigma_model;
}
arma::Mat<double> EKF::get_P_pred()
{
	return P_pred;
}
void EKF::set_P_pred(arma::Mat<double> new_P_pred)
{
	P_pred = new_P_pred;
}
arma::Mat<double> EKF::get_H_MSG()
{
	return H_MSG;
}
void EKF::set_H_MSG(arma::Mat<double> new_H_MSG)
{
	H_MSG = new_H_MSG;
}
arma::Mat<double> EKF::get_H_MG()
{
	return H_MG;
}
void EKF::set_H_MG(arma::Mat<double> new_H_MG)
{
	H_MG = new_H_MG;
}
arma::Mat<double> EKF::get_K_MSG()
{
	return K_MSG;
}
void EKF::set_K_MSG(arma::Mat<double> new_K_MSG)
{
	K_MSG = new_K_MSG;
}
arma::Mat<double> EKF::get_K_MG()
{
	return K_MG;
}
void EKF::set_K_MG(arma::Mat<double> new_K_MG)
{
	K_MG = new_K_MG;
}
arma::Col<double> EKF::get_Z_MSG()
{
	return Z_MSG;
}
void EKF::set_Z_MSG(arma::Col<double> new_Z_MSG)
{
	Z_MSG = new_Z_MSG;
}
arma::Col<double> EKF::get_Z_MG()
{
	return Z_MG;
}
void EKF::set_Z_MG(arma::Col<double> new_Z_MG)
{
	Z_MG = new_Z_MG;
}
arma::Col<double> EKF::get_h_MSG()
{
	return h_MSG;
}
void EKF::set_h_MSG(arma::Col<double> new_h_MSG)
{
	h_MSG = new_h_MSG;
}
arma::Col<double> EKF::get_h_MG()
{
	return h_MG;
}
void EKF::set_h_MG(arma::Col<double> new_h_MG)
{
	h_MG = new_h_MG;
}
arma::Mat<double> EKF::get_J()
{
	return J;
}
void EKF::set_J(arma::Mat<double> new_J)
{
	J = new_J;
}
arma::Mat<double> EKF::get_J_inv()
{
	return J_inv;
}
void EKF::set_J_inv(arma::Mat<double> new_J_inv)
{
	J_inv = new_J_inv;
}
arma::Mat<double> EKF::get_delta_J_cycl()
{
	return delta_J_cycl;
}
void EKF::set_delta_J_cycl(arma::Mat<double> new_delta_J_cycl)
{
	delta_J_cycl = new_delta_J_cycl;
}
double EKF::get_sigma_torque()
{
	return sigma_torque;
}
void EKF::set_sigma_torque(double new_sigma_torque)
{
	sigma_torque = new_sigma_torque;
}
double EKF::get_sigma_mres()
{
	return sigma_mres;
}
void EKF::set_sigma_mres(double new_sigma_mres)
{
	sigma_mres = new_sigma_mres;
}
double EKF::get_sigma_mm_bias()
{
	return sigma_mm_bias;
}
void EKF::set_sigma_mm_bias(double new_sigma_mm_bias)
{
	sigma_mm_bias = new_sigma_mm_bias;
}
double EKF::get_sigma_gyr_bias()
{
	return sigma_gyr_bias;
}
void EKF::set_sigma_gyr_bias(double new_sigma_gyr_bias)
{
	sigma_gyr_bias = new_sigma_gyr_bias;
}
double EKF::get_sigma_magn()
{
	return sigma_magn;
}
void EKF::set_sigma_magn(double new_sigma_magn)
{
	sigma_magn = new_sigma_magn;
}
double EKF::get_sigma_sun()
{
	return sigma_sun;
}
void EKF::set_sigma_sun(double new_sigma_sun)
{
	sigma_sun = new_sigma_sun;
}
double EKF::get_sigma_gyr()
{
	return sigma_gyr;
}
void EKF::set_sigma_gyr(double new_sigma_gyr)
{
	sigma_gyr = new_sigma_gyr;
}
arma::Mat<double> EKF::get_Sigma_process()
{
	return Sigma_process;
}
void EKF::set_Sigma_process(arma::Mat<double> new_Sigma_process)
{
	Sigma_process = new_Sigma_process;
}
arma::Mat<double> EKF::get_G()
{
	return G;
}
void EKF::set_G(arma::Mat<double> new_G)
{
	G = new_G;
}
arma::Mat<double> EKF::get_Sigma_obs_MSG()
{
	return Sigma_obs_MSG;
}
void EKF::set_Sigma_obs_MSG(arma::Mat<double> new_Sigma_obs_MSG)
{
	Sigma_obs_MSG = new_Sigma_obs_MSG;
}
arma::Mat<double> EKF::get_Sigma_obs_MG()
{
	return Sigma_obs_MG;
}
void EKF::set_Sigma_obs_MG(arma::Mat<double> new_Sigma_obs_MG)
{
	Sigma_obs_MG = new_Sigma_obs_MG;
}
arma::Col<double> EKF::get_X()
{
	return X;
}
void EKF::set_X(arma::Col<double> new_X)
{
	X = new_X;
}
bool EKF::get_shadow()
{
	return inShadow;
}
void EKF::set_shadow(bool new_shadow)
{
	inShadow = new_shadow;
}
arma::Col<double> EKF::get_B_body()
{
	return B_body;
}
void EKF::set_B_body(arma::Col<double> new_B_body)
{
	B_body = new_B_body;
}

void EKF::set_sigmas_init(double s_q0, double s_w0, double s_mres0, double s_mmbias0, double s_gyrbias0, double s_torque, double s_mres, double s_mm_bias, double s_gyr_bias, double s_magn, double s_sun, double s_gyr)
{
	set_sigma_q0(s_q0);
	set_sigma_omega0(s_w0);
	set_sigma_mres0(s_mres0);
	set_sigma_mmbias0(s_mmbias0);
	set_sigma_gyrbias0(s_gyrbias0);
	set_sigma_torque(s_torque);
	set_sigma_mres(s_mres);
	set_sigma_mm_bias(s_mm_bias);
	set_sigma_gyr_bias(s_gyr_bias);
	set_sigma_magn(s_magn);
	set_sigma_sun(s_sun);
	set_sigma_gyr(s_gyr);
}
void EKF::set_P_init()
{
	arma::Mat<double> new_P(15, 15, arma::fill::zeros);
	new_P(0, 0) = new_P(1, 1) = new_P(2, 2) = pow(sigma_q0, 2);
	new_P(3, 3) = new_P(4, 4) = new_P(5, 5) = pow(sigma_omega0, 2);
	new_P(6, 6) = new_P(7, 7) = new_P(8, 8) = pow(sigma_mres0, 2);
	new_P(9, 9) = new_P(10, 10) = new_P(11, 11) = pow(sigma_mmbias0, 2);
	new_P(12, 12) = new_P(13, 13) = new_P(14, 14) = pow(sigma_gyrbias0, 2);
	set_P_corr(new_P);
	set_P_pred(new_P);
}
void EKF::set_J_init(arma::Col<double> J_vec)
{
	set_J(arma::diagmat(J_vec));
	set_J_inv(arma::inv_sympd(J));

	set_delta_J_cycl(arma::Mat<double> { { J(1, 1) - J(2, 2), 0, 0 }, {0, J(2, 2) - J(0, 0), 0 }, { 0, 0, J(0, 0) - J(1, 1) } });

	arma::Mat<double> new_G(15, 12, arma::fill::zeros);
	new_G.submat(3, 0, 5, 2) = J_inv;
	new_G.submat(6, 3, 14, 11) = arma::Mat<double>(9, 9, arma::fill::eye);
	set_G(new_G);
}
void EKF::set_Sigma_init()
{
	arma::Mat<double> new_Sigma_process(12, 12, arma::fill::zeros);
	new_Sigma_process(0, 0) = new_Sigma_process(1, 1) = new_Sigma_process(2, 2) = pow(sigma_torque, 2);
	new_Sigma_process(3, 3) = new_Sigma_process(4, 4) = new_Sigma_process(5, 5) = pow(sigma_mres, 2);
	new_Sigma_process(6, 6) = new_Sigma_process(7, 7) = new_Sigma_process(8, 8) = pow(sigma_mm_bias, 2);
	new_Sigma_process(9, 9) = new_Sigma_process(10, 10) = new_Sigma_process(11, 11) = pow(sigma_gyr_bias, 2);
	set_Sigma_process(new_Sigma_process);

	arma::Mat<double> new_S_MSG(9, 9, arma::fill::zeros);
	new_S_MSG(0, 0) = new_S_MSG(1, 1) = new_S_MSG(2, 2) = pow(sigma_magn, 2);
	new_S_MSG(3, 3) = new_S_MSG(4, 4) = new_S_MSG(5, 5) = pow(sigma_sun, 2);
	new_S_MSG(6, 6) = new_S_MSG(7, 7) = new_S_MSG(8, 8) = pow(sigma_gyr, 2);
	set_Sigma_obs_MSG(new_S_MSG);

	arma::Mat<double> new_S_MG(6, 6, arma::fill::zeros);
	new_S_MG(0, 0) = new_S_MG(1, 1) = new_S_MG(2, 2) = pow(sigma_magn, 2);
	new_S_MG(3, 3) = new_S_MG(4, 4) = new_S_MG(5, 5) = pow(sigma_gyr, 2);
	set_Sigma_obs_MG(new_S_MG);
}

void EKF::update_F(double T, arma::Col<double> moment_prev)
{
	arma::Col<double> e3_body = rot_q(X.subvec(0, 3), arma::Col<double>{0, 0, 1});

	arma::Mat<double> new_F(15, 15, arma::fill::zeros);

	new_F.submat(0, 0, 2, 2) = -skew_symm(X.subvec(4, 6));
	new_F.submat(0, 3, 2, 5) = arma::Mat<double>(3, 3, arma::fill::eye) * 0.5;
	new_F.submat(3, 0, 5, 2) = J_inv * (2 * skew_symm(moment_prev) * skew_symm(B_body) - 6 * pow(mean_motion, 2) * delta_J_cycl * symm(e3_body) * skew_symm(e3_body));
	new_F.submat(3, 3, 5, 5) = J_inv * delta_J_cycl * symm(X.subvec(4, 6));
	new_F.submat(3, 6, 5, 8) = -J_inv * skew_symm(B_body);

	set_F(new_F);
}

void EKF::update_Fi_and_Sigma_model(double T)
{
	set_Fi(F * T + arma::Mat<double>(15, 15, arma::fill::eye));

	set_Sigma_model(Fi * G * Sigma_process * G.t() * Fi.t() * T);
}

void EKF::update_P_pred()
{
	set_P_pred(Fi * P_corr * Fi.t() + Sigma_model);
}
void EKF::update_H_MSG(arma::Col<double> B_body_model, arma::Col<double> S_body_model)
{
	arma::Mat<double> W1 = skew_symm(B_body_model);
	arma::Mat<double> W2 = skew_symm(S_body_model);

	arma::Mat<double> new_H_MSG(9, 15, arma::fill::zeros);
	new_H_MSG.submat(0, 0, 2, 2) = 2 * W1;
	new_H_MSG.submat(3, 0, 5, 2) = 2 * W2;
	new_H_MSG.submat(6, 3, 8, 5) = arma::Mat<double>(3, 3, arma::fill::eye);
	new_H_MSG.submat(0, 9, 2, 11) = arma::Mat<double>(3, 3, arma::fill::eye);
	new_H_MSG.submat(6, 12, 8, 14) = arma::Mat<double>(3, 3, arma::fill::eye);

	set_H_MSG(new_H_MSG);
}
void EKF::update_H_MG(arma::Col<double> B_body_model)
{
	arma::Mat<double> W = skew_symm(B_body_model);

	arma::Mat<double> new_H_MG(6, 15, arma::fill::zeros);
	new_H_MG.submat(0, 0, 2, 2) = 2 * W;
	new_H_MG.submat(3, 3, 5, 5) = arma::Mat<double>(3, 3, arma::fill::eye);
	new_H_MG.submat(0, 9, 2, 11) = arma::Mat<double>(3, 3, arma::fill::eye);
	new_H_MG.submat(3, 12, 5, 14) = arma::Mat<double>(3, 3, arma::fill::eye);

	set_H_MG(new_H_MG);
}

void EKF::update_K_MSG()
{
	arma::Mat<double> H_MSG_t = H_MSG.t();
	set_K_MSG(P_pred * H_MSG_t * (H_MSG * P_pred * H_MSG_t + Sigma_obs_MSG).i());
}
void EKF::update_K_MG()
{
	arma::Mat<double> H_MG_t = H_MG.t();
	set_K_MG(P_pred * H_MG_t * (H_MG * P_pred * H_MG_t + Sigma_obs_MG).i());
}
void EKF::update_Z_MSG(arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas)
{
	set_Z_MSG(arma::join_cols(B_meas, S_meas, omega_meas));
}
void EKF::update_Z_MG(arma::Col<double> B_meas, arma::Col<double> omega_meas)
{
	set_Z_MG(arma::join_cols(B_meas, omega_meas));
}
void EKF::update_h_MSG(arma::Col<double> B_body_model, arma::Col<double> S_body_model, arma::Col<double> omega_body_model)
{
	set_h_MSG(arma::join_cols(B_body_model, S_body_model, omega_body_model));
}
void EKF::update_h_MG(arma::Col<double> B_body_model, arma::Col<double> omega_body_model)
{
	set_h_MG(arma::join_cols(B_body_model, omega_body_model));
}
void EKF::update_P_corr_MSG()
{
	set_P_corr((arma::Mat<double>(15, 15, arma::fill::eye) - K_MSG * H_MSG) * P_pred);
}
void EKF::update_P_corr_MG()
{
	set_P_corr((arma::Mat<double>(15, 15, arma::fill::eye) - K_MG * H_MG) * P_pred);
}
void EKF::update_X_MSG(arma::Col<double> X_extr)
{
	arma::Col<double> new_X(16, arma::fill::zeros);

	arma::Col<double> delta_X = K_MSG * (Z_MSG - h_MSG);

	new_X.subvec(4, 6) = X_extr.subvec(4, 15) + delta_X.subvec(3, 14);

	arma::Col<double> delta_q = arma::normalise(arma::join_cols(arma::Col<double>({ 1 }), delta_X.subvec(0, 2)));

	new_X.subvec(0, 3) = quat_product(X_extr.subvec(0, 3), delta_q);

	set_X(new_X);
}
void EKF::update_X_MG(arma::Col<double> X_extr)
{
	arma::Col<double> new_X(16, arma::fill::zeros);

	arma::Col<double> delta_X = K_MG * (Z_MG - h_MG);

	new_X.subvec(4, 6) = X_extr.subvec(4, 15) + delta_X.subvec(3, 14);

	arma::Col<double> delta_q = arma::normalise(arma::join_cols(arma::Col<double>({ 1 }), delta_X.subvec(0, 2)));

	new_X.subvec(0, 3) = quat_product(X_extr.subvec(0, 3), delta_q);

	set_X(new_X);
}
void EKF::correct_state_MSG(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> S_orb_model, arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev)
{
	update_F(T, moment_prev);
	update_Fi_and_Sigma_model(T);
	update_P_pred();
	arma::Col<double> B_body_model = rot_q(X_extr.subvec(0, 3), B_orb_model);
	arma::Col<double> S_body_model = rot_q(X_extr.subvec(0, 3), S_orb_model);
	update_H_MSG(B_body_model, S_body_model);
	update_K_MSG();
	update_Z_MSG(B_meas, S_meas, omega_meas);
	update_h_MSG(B_body_model, S_body_model, X_extr.subvec(4, 6));
	update_P_corr_MSG();
	update_X_MSG(X_extr);
	set_B_body(rot_q(X.subvec(0, 3), B_orb_model));
}
void EKF::correct_state_MG(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> B_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev)
{
	update_F(T, moment_prev);
	update_Fi_and_Sigma_model(T);
	update_P_pred();
	arma::Col<double> B_body_model = rot_q(X_extr.subvec(0, 3), B_orb_model);
	update_H_MG(B_body_model);
	update_K_MG();
	update_Z_MG(B_meas, omega_meas);
	update_h_MG(B_body_model, X_extr.subvec(4, 6));
	update_P_corr_MG();
	update_X_MG(X_extr);
	set_B_body(rot_q(X.subvec(0, 3), B_orb_model));
}
void EKF::update_shadow(arma::Col<double> S_model, arma::Col<double> R)
{
	arma::Col<double> sum = R + S_model * 1382864108.1;

	double dot = arma::dot(sum, S_model);

	bool a = dot > 0;
	bool b = dot < 1382834756.3;
	bool c = acos(arma::dot(arma::normalise(sum), S_model)) < 0.0046071212;

	set_shadow(a && b && c);
}
void EKF::correct_state(arma::Col<double> X_extr, double T, arma::Col<double> B_orb_model, arma::Col<double> S_orb_model, arma::Col<double> B_meas, arma::Col<double> S_meas, arma::Col<double> omega_meas, arma::Col<double> moment_prev, arma::Col<double> R_orb)
{
	update_shadow(S_orb_model, R_orb);
	if (inShadow)
	{
		correct_state_MG(X_extr, T, B_orb_model, B_meas, omega_meas, moment_prev);
	}
	else
	{
		correct_state_MSG(X_extr, T, B_orb_model, S_orb_model, B_meas, S_meas, omega_meas, moment_prev);
	}
}
