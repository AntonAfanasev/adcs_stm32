/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "fatfs.h"
#include "calculation.h"
#include <armadillo>
#include "EKF.h"
#include "math_utils.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define DELAY_PWM 70
#define DELAY_NOPWM 30
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim3;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;

/* USER CODE BEGIN PV */
UART_HandleTypeDef * muart = &huart2;
int16_t magn_x, magn_y, magn_z, gyro_x, gyro_y, gyro_z, ascil_x, ascil_y, ascil_z, unc_mx, unc_my, unc_mz;
float gyroCalibZ = 0;
float voltageX = 0, voltageY = 0;
float calibDataMagnet[3];
int pwm_x = 0, pwm_y = 0, pwm_z = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
uint8_t sect[256];
char wtext[512] ="";
//char wtext[512] ="magn_x magn_y magn_z gyro_x gyro_y gyro_z ascil_x ascil_y ascil_z angle pwm_x pwm_y pwm_z\n\r";
float currentAngle;
int velocity;
extern char str1[60];
uint32_t byteswritten,bytesread;
uint8_t result;
extern char USERPath[4]; /* logical drive path */
FATFS SDFatFs;
FATFS *fs;
FIL MyFile;

FILINFO fileInfo;
char *fn;
DIR dir;
DWORD fre_clust, fre_sect, tot_sect;


int sdcard_test_stat()
{
  if(f_mount(&SDFatFs,(TCHAR const*)USERPath,0)!=FR_OK)
  {
	Error_Handler();
  }
  else
  {
	//fileInfo.fname = (char*)sect;
	strcpy(fileInfo.fname, (char *)sect);
	fileInfo.fsize = sizeof(sect);
	result = f_opendir(&dir, "/");
	if (result == FR_OK)
	{
		while(1)
		  {
			result = f_readdir(&dir, &fileInfo);
			if (result==FR_OK && fileInfo.fname[0])
			{
				fn = fileInfo.fname;
				  if(strlen(fn)) HAL_UART_Transmit(muart,(uint8_t*)fn,strlen(fn),0x1000);
				  else HAL_UART_Transmit(muart,(uint8_t*)fileInfo.fname,strlen((char*)fileInfo.fname),0x1000);
				  if(fileInfo.fattrib&AM_DIR)
				  {
					HAL_UART_Transmit(muart,(uint8_t*)" [DIR]",7,0x1000);
				  }
			}
			else break;
			HAL_UART_Transmit(muart,(uint8_t*)"\r\n",2,0x1000);
		  }

		//-------------------------------------------
		f_getfree("/", &fre_clust, &fs);
		sprintf(str1,"\nfre_clust: %lu\r\n",fre_clust);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1,"n_fatent: %lu\r\n",fs->n_fatent);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1,"fs_csize: %d\r\n",fs->csize);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		tot_sect = (fs->n_fatent - 2) * fs->csize;
		sprintf(str1,"tot_sect: %lu\r\n",tot_sect);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		fre_sect = fre_clust * fs->csize;
		sprintf(str1,"fre_sect: %lu\r\n",fre_sect);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		sprintf(str1, "%lu KB total drive space.\r\n%lu KB available.\r\n",
		fre_sect/2, tot_sect/2);
		HAL_UART_Transmit(muart,(uint8_t*)str1,strlen(str1),0x1000);
		//-------------------------------------------

	  f_closedir(&dir);
	}
  }
  //FATFS_UnLinkDriver(USERPath);
  HAL_Delay(1000);
  return 0;
}

int pwm_set(int a, int b, int c) //�� -1024 �� 1024
{
	/*TIM_OC_InitTypeDef sConfigOC = {0};
	sConfigOC.OCMode = TIM_OCMODE_PWM1;

	sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;


*/
	int x = a; //Set sensor axises in accordance with satellite axises
	int y = b;
	int z = c;
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_RESET);
	if (x<0)
	{
		TIM3->CCR1 = 1024 + x;
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2, GPIO_PIN_SET);
	}
	else TIM3->CCR1 = x;
	//HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1);

	if (y<0)
	{
		TIM3->CCR2 = 1024 + y;
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
	}
	else TIM3->CCR2 = y;
	//HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2);

	if (z<0)
	{
		TIM3->CCR3 = 1024 + z;
		HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET);
	}
	else TIM3->CCR3 = z;
	//HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3);
	//HAL_TIM_MspPostInit(&htim3);
	//HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_1);//�������� ��� 1 �������
	//HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_2);//�������� ��� 2 �������
	//HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_3);//�������� ��� 3 �������
	return 0;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();
  MX_FATFS_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
  HAL_UART_Transmit(muart,(uint8_t*)"HELLO!\n\r",8,0xff);

  initImu(&hi2c1, muart);

  disk_initialize(SDFatFs.drv);
  sdcard_test_stat();
  FIL file;
  FRESULT res;
  const TCHAR filename[] = "my_telemetry.txt";

  //if(f_open(&file,filename,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
  //{
//	  HAL_UART_Transmit(muart,(uint8_t*)"SD-CARD ERROR\n\r", 15, 0xff);
//	  Error_Handler();
  //}

  //HAL_UART_Transmit(muart,(uint8_t*)wtext,strlen((char *)wtext),0xff);

  //res=f_write(&file,wtext,strlen((char *)wtext),(void*)&byteswritten);
  //if((byteswritten==0)||(res!=FR_OK))
  //{
	  //HAL_UART_Transmit(muart,(uint8_t*)"SD-CARD ERROR\n\r", 15, 0xff);
	  //Error_Handler();
  //}

  int lines_count = 0;
  uint32_t timestamp = HAL_GetTick();
  int is_pwm_on = 0;


  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_SET); //enable ���������

  ////////////////////////////

  HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_1);//�������� ��� 1 �������
  HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_2);//�������� ��� 2 �������
  HAL_TIM_PWM_Start (&htim3, TIM_CHANNEL_3);//�������� ��� 3 �������

  ////////////////////////////
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  getValue(&magn_x, &magn_y, &magn_z, &gyro_x, &gyro_y, &gyro_z, &ascil_x, &ascil_y, &ascil_z, &unc_mx, &unc_my, &unc_mz);
  HAL_UART_Transmit(muart,(uint8_t*)"start\n\r", 7, 0xff);
  velocity = 0;

  EKF* ekf = new EKF;

  ekf->set_sigmas_init(arma::datum::pi / 2, 1e-2, 5e-3, 1e-5, 1e-5, 5e-8, 1e-7, 1e-6, 1e-6, 1e-7, 1e-5, 1e-3);
  ekf->set_P_init();
  ekf->set_J_init(arma::Col<double>{0.014, 0.015, 0.007});
  ekf->set_Sigma_init();

  double Kw = 6000;
  double Ks = 80;
  double maxVol = 2.5;

  arma::Col<double> X_pred = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  ekf->set_X(X_pred);

  arma::Col<double> m_moment = {0,0,0};
  arma::Col<double> m_scaled = {0,0,0};
  arma::Col<double> B_body_model = {0,0,0};

  double GyroX = 0;
  double GyroY = 0;
  double GyroZ = 0;

  double mX = 0;
  double mY = 0;
  double mZ = 0;


  while (1)
  {
	  X_pred = solve_motion_equation(ekf->get_X(), 0.2, 10, 0, ekf->get_sigma_torque(), ekf->get_J(), ekf->get_J_inv(), arma::Col<double>{0,0,0}, arma::Col<double>{0,0,0}, false);

	  /*sprintf(wtext, "\n Mx=%6i My=%6i GyroZ=%6i Px=%6i Py%6i\r\n", magn_x, magn_y, int(X_pred(0) * 1000), int(X_pred(1) * 1000), int(X_pred(2) * 1000));

	  while( HAL_UART_Transmit_IT(&huart2,(uint8_t*)wtext,54) == HAL_BUSY ); //this line execution takes 0 ms!!!!!!!
	  HAL_UART_Transmit(muart,(uint8_t*)"step\n\r", 6, 0xff);*/

	  //+2ms from while(1) start
	  //read I2C data
	  getValue(&magn_x, &magn_y, &magn_z, &gyro_x, &gyro_y, &gyro_z, &ascil_x, &ascil_y, &ascil_z, &unc_mx, &unc_my, &unc_mz);
	  //play with numbers
	  currentAngle = ((atan2(-magn_y, magn_x)) * 180) / 3.1415926;
	  GyroZ = (double)gyro_z*8.75/1000*3.1415926/180; // convert omega from bits to radians
	  GyroY = (double)gyro_y*8.75/1000*3.1415926/180; // convert omega from bits to radians
	  GyroX = (double)gyro_x*8.75/1000*3.1415926/180; // convert omega from bits to radians

	  mX = (double)magn_x / 1e7;
	  mY = (double)magn_y / 1e7;
	  mZ = (double)magn_z / 1e7;

	  double magn_xy = mX*mX + mY*mY;
	  arma::Col<double> magn_r = arma::Col<double>{sqrt(magn_xy)/sqrt(2), sqrt(magn_xy)/sqrt(2), mZ};
	  B_body_model = rot_q(X_pred.subvec(0,3), magn_r);
	  m_moment = produce_moment(Kw, Ks, 0, 4 * X_pred(0) * X_pred.subvec(1,3), arma::Col<double>{GyroX - X_pred(13), GyroY - X_pred(14), GyroZ - X_pred(15)}, B_body_model, X_pred.subvec(7,9));
	  m_scaled = moment_scale(m_moment, maxVol);

	  sprintf(wtext, "\nm1=%6i m2=%6i m3=%6i q0=%6i q1=%6i q2=%6i q3=%6i b1=%6i b2=%6i b3=%6i\r", int(m_scaled(0) * 1000), int(m_scaled(1) * 1000), int(m_scaled(2) * 1000), int(X_pred(0) * 1000), int(X_pred(1) * 1000), int(X_pred(2) * 1000), int(X_pred(3) * 1000), int(B_body_model(0) * 1000), int(B_body_model(1) * 1000), int(B_body_model(2) * 1000));

	  while( HAL_UART_Transmit_IT(&huart2,(uint8_t*)wtext,104) == HAL_BUSY ); //this line execution takes 0 ms!!!!!!!
	  HAL_UART_Transmit(muart,(uint8_t*)"step\n\r", 6, 0xff);

	  X_pred = solve_motion_equation(X_pred, 0.8, 10, 0, ekf->get_sigma_torque(), ekf->get_J(), ekf->get_J_inv(), m_scaled, B_body_model, true);
	  //X_pred = solve_motion_equation(X_pred, 0.8, 10, 0, ekf->get_sigma_torque(), ekf->get_J(), ekf->get_J_inv(), arma::Col<double>{0,0,0}, B_body_model, true);

	  //getVoltage(gyroCalibZ, calibDataMagnet, currentAngle*3.1415926/180, &voltageX, &voltageY);
	  //scaleVoltage(&voltageX, &voltageY);

	  pwm_x = (int)(m_scaled(0)/maxVol*1023);
	  pwm_y = (int)(m_scaled(1)/maxVol*1023); //+4ms from while(1) start

	  ekf->correct_state_MG(X_pred, 1, magn_r, arma::Col<double>{mX - X_pred(10), mY - X_pred(11), mZ - X_pred(12)}, arma::Col<double>{(double)GyroX - X_pred(13), (double)GyroY - X_pred(14), (double)GyroZ - X_pred(15)}, m_scaled + X_pred.subvec(7,9));

	  //send data by Bluetooth

	  //sprintf(wtext, "\nMx=%6i My=%6i quat0=%6i q1=%6i q2=%6i\r\n", magn_x, magn_y, int(X_pred(0) * 1000), int(X_pred(1) * 1000), int(X_pred(2) * 1000));
	  //sprintf(wtext, "\n Mx=%6i My=%6i GyroZ=%6i Px=%6i Py%6i\r\n", magn_x, magn_y, int(GyroZ * 1000), pwm_x, pwm_y);
//	  sprintf(wtext, "%6i;%6i;%6i\r\n", unc_mx, unc_my, unc_mz);
	  //sprintf(wtext, "%6i;%6i;%6i\t%6i;%6i;%6i\t%3i\r\n", magn_x, magn_y, magn_z, unc_mx, unc_my, unc_mz, (int)currentAngle);

	 // HAL_UART_Transmit(muart,(uint8_t*)wtext,strlen((char *)wtext),0xff); //this line execution takes 96 ms!!!!!!!
	//  HAL_UART_AbortTransmit(&huart2);
	  //while( HAL_UART_Transmit_IT(&huart2,(uint8_t*)wtext,54) == HAL_BUSY ); //this line execution takes 0 ms!!!!!!!
	  //HAL_UART_Transmit(muart,(uint8_t*)"step\n\r", 6, 0xff);

//	  res=f_write(&file,wtext,strlen((char *)wtext),(void*)&byteswritten);
//	  if((byteswritten==0)||(res!=FR_OK))
//	  {
//		  HAL_UART_Transmit(muart,(uint8_t*)"SD-CARD ERROR 1\n\r", 17, 0xff);
//	  	  Error_Handler();
//	  }
//
//	  lines_count++;
//
//	  //Save to micro SD
//	  if (lines_count >= 1000)
//	  {
//		  f_close(&file);
//
//		  if(f_open(&file,filename,FA_OPEN_APPEND|FA_WRITE)!=FR_OK) //FA_OPEN_APPEND
//		  {
//			  HAL_UART_Transmit(muart,(uint8_t*)"SD-CARD ERROR 2\n\r", 17, 0xff);
//			  Error_Handler();
//		  }
//
//		  lines_count = 0;
//	  }


	  //pwm_set(1023,1023,0);
	  pwm_set(pwm_x,pwm_y,0);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
	  HAL_Delay(800);
	  pwm_set(0,0,0);
	  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
	  HAL_Delay(200);

	  /*sprintf(wtext, "\nMx=%6i My=%6i quat0=%6i q1=%6i q2=%6i\r\n", magn_x, magn_y, int(X_pred(0) * 1000), int(X_pred(1) * 1000), int(X_pred(2) * 1000));
	  while( HAL_UART_Transmit_IT(&huart2,(uint8_t*)wtext,54) == HAL_BUSY ); //this line execution takes 0 ms!!!!!!!
	  HAL_UART_Transmit(muart,(uint8_t*)"step\n\r", 6, 0xff);*/

/*
	  //PWM cycle
	  if ((HAL_GetTick() - timestamp > DELAY_PWM + DELAY_NOPWM)&&(is_pwm_on == 0))
	  {
		  timestamp = HAL_GetTick();
//		  pwm_x = 0001;
//		  pwm_y = 0001;
		  pwm_z = 0;
		  pwm_set(pwm_x, pwm_y, pwm_z);
		  is_pwm_on = 1;
		  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
	  }
	  else
		  if ((HAL_GetTick() - timestamp > DELAY_PWM)&&(is_pwm_on == 1))
		  {
			  pwm_x = 0;
			  pwm_y = 0;
			  pwm_z = 0;
			  pwm_set(pwm_x, pwm_y, pwm_z);
			  is_pwm_on = 0;
			  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_SET);
		  }
	  HAL_Delay(10);
*/
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 40;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00909BEB;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 7;
  hspi1.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi1.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 80;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1024;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_PWM_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 256;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_ENABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 512;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.Pulse = 768;
  if (HAL_TIM_PWM_ConfigChannel(&htim3, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */
  HAL_TIM_MspPostInit(&htim3);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 9600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  HAL_PWREx_EnableVddIO2();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, GPIO_PIN_6, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(USB_PowerSwitchOn_GPIO_Port, USB_PowerSwitchOn_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4, GPIO_PIN_RESET);

  /*Configure GPIO pin : PE6 */
  GPIO_InitStruct.Pin = GPIO_PIN_6;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_OverCurrent_Pin */
  GPIO_InitStruct.Pin = USB_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USB_OverCurrent_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : USB_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = USB_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(USB_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : STLK_RX_Pin STLK_TX_Pin */
  GPIO_InitStruct.Pin = STLK_RX_Pin|STLK_TX_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pin : PA8 */
  GPIO_InitStruct.Pin = GPIO_PIN_8;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : USB_ID_Pin USB_DM_Pin USB_DP_Pin */
  GPIO_InitStruct.Pin = USB_ID_Pin|USB_DM_Pin|USB_DP_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF10_OTG_FS;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : PD2 PD3 PD4 */
  GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3|GPIO_PIN_4;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
	NVIC_SystemReset();
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(char *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
